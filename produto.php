<?php
	$conexao = mysqli_connect("127.0.0.1", "root", "seph", "livro");
	$dados = mysqli_query($conexao, "SELECT * FROM livro WHERE id = $_GET[id]");
	$produto = mysqli_fetch_array($dados);
	?>
		<body>
			<?php 
				$cabecalho_title = "Produto da Mirror fashion";
				include("cabecalho.php"); 
			?>
		<div class="produto-back">
		<div class="container">
			<div class="produto">
				<h1><?= $produto["title"] ?></h1>
				<form>
					<fieldset class="cores">
						
						<label for="verde">
							<img src="img/produtos/foto<?= $produto["id"] ?>-verde.png" alt="Produto na cor verde">
						</label>
					</fieldset>
				</form>
			
			</div>
			<div class="detalhes">
				<table>
					<thead>
						<tr>
							<th>Característica</th>
							<th>Detalhe</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>ID</td>
							<td><?= $produto["id"] ?></td>
						</tr>
						<tr>
							<td>Titulo</td>
							<td><?= $produto["title"] ?></td>
						</tr>
						<tr>
							<td>Autor</td>
							<td><?= $produto["author"] ?></td>
						</tr>
						<tr>
							<td>Descrição</td>
							<td><?= $produto["description"] ?></td>
						</tr>
						<tr>
							<td>releaseDate</td>
							<td><?= $produto["releaseDate"] ?></td>
						</tr>
						<tr>
							<td>isbn</td>
							<td><?= $produto["isbn"] ?></td>
						</tr>
						<tr>
							<td>Ano</td>
							<td><?= $produto["year"] ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
 <div id="HCB_comment_box"><a href="http://www.htmlcommentbox.com">HTML Comment Box</a> is loading comments...</div>
 <link rel="stylesheet" type="text/css" href="http://www.htmlcommentbox.com/static/skins/default/skin.css" />
 <script type="text/javascript" language="javascript" id="hcb"> /*<!--*/ if(!window.hcb_user){hcb_user={  };} (function(){s=document.createElement("script");s.setAttribute("type","text/javascript");s.setAttribute("src", "http://www.htmlcommentbox.com/jread?page="+escape((window.hcb_user && hcb_user.PAGE)||(""+window.location)).replace("+","%2B")+"&opts=470&num=10");if (typeof s!="undefined") document.getElementsByTagName("head")[0].appendChild(s);})(); /*-->*/ </script>
		</div>
			<?php include("rodape.php"); ?>
		</body>

</html>
