		<?php 
			$cabecalho_title = "Homepage da Mirror Fashion";
			include ("cabecalho.php"); ?>

		<section id="main">
			<div class="container destaque">
				<!--<section class="busca">
					<h2>BUSCA</h2>
					<form action="http://www.google.com.br/search" id="form-busca">
						<input type="search" name="q" id="q">
						<input type="image" src="img/busca.png">
					</form>
				</section> 
					<section class="menu-departamentos">
						<h2>DEPARTAMENTOS</h2>
						<nav>
							<ul>
								<li><a href="#">BLUSAS E CAMISAS</a>
								<ul>
									<li><a href="#">Manga curta</a></li>
									<li><a href="#">Manga comprida</a></li>
									<li><a href="#">Regatas</a></li>
									<li><a href="#">Gola em V</a></li>
								</ul>
								</li>
								<li><a href="#">CALÇAS</a></li>
								<li><a href="#">SAIAS</a></li>
								<li><a href="#">VESTIDOS</a></li>
								<li><a href="#">SAPATOS</a></li>
								<li><a href="#">BOLSAS E CARTEIRAS</a></li>
								<li><a href="#">ACESSÓRIOS</a></li>
							</ul>
						</nav>
					</section>
				<img src="img/livro-homepage.png" alt="Imagem de livros diversos">
			</div>
		</section> -->

		<section id="destaques">
			<div class="container paineis">
				<section class="painel novidades">
					<h2>LIVROS</h2>
					<ol>
						<?php
							$conexao = mysqli_connect("127.0.0.1", "root", "seph", "livro");
							$dados = mysqli_query($conexao, "SELECT * FROM livro");
							while ($produto = mysqli_fetch_array($dados)):
						?>

						<li>
							<a href="produto.php?id=<?= $produto[id] ?>">
								<figure>
									<img src="img/produtos/miniatura<?= $produto[id] ?>.png"
										alt="<?= $produto[title] ?>">
									<figcaption> Título: <?= $produto[title] ?>, Autor: <?= $produto[author] ?>, Descrição: <?= $produto[description] ?></figcaption>
								</figure>
							</a>
						</li>
					<?php endwhile; ?>
					</ol>	
				</section>
<!--
				<section class="painel mais-vendidos">
					<h2>MAIS VENDIDOS</h2>
					<ol>
						
						<li>
							<a href="produto.html">
								<figure>
									<img src="img/produtos/miniatura8.png">
									<figcaption>Fuzz Cardigan por R$129,90</figcaption>
								</figure>
							</a>
						</li>

						
						<li>
							<a href="produto.html">
							<figure>
								<img src="img/produtos/miniatura9.png">
								<figcaption>Produto X por R$99,90</figcaption>
							</figure>
						</a>

						</li>

						<li>
							<a href="produto.html">
							<figure>
								<img src="img/produtos/miniatura10.png">
								<figcaption>Produto Y por R$139,90</figcaption>
							</figure>
							</a>
						</li>
						
						<li>
							<a href="produto.html">
							<figure>
								<img src="img/produtos/miniatura11.png">
								<figcaption>Produto Z por R$149,90</figcaption>
							</figure>
							</a>
						</li>

						<li>
							<a href="produto.html">
							<figure>
								<img src="img/produtos/miniatura12.png">
								<figcaption>Produto XPTO por R$1890,90</figcaption>
							</figure>
							</a>
						</li>

						<li>
							<a href="produto.html">
							<figure>
								<img src="img/produtos/miniatura13.png">
								<figcaption>Produto Blabla por R$690,90</figcaption>
							</figure>
							</a>
						</li>

						<li>
							<a href="produto.html">
							<figure>
								<img src="img/produtos/miniatura14.png">
								<figcaption>Produto Teste por R$690,90</figcaption>
							</figure>
							</a>
						</li>

					</ol>	
				</section>



			</div>
		</section> -->

		<?php include("rodape.php"); ?>
	<script src="js/home.js"></script>
	</body>
</html>